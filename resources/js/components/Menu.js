import React, { useState } from "react";

function Menu() {
    const [menuStatus, setMenu] = useState(false);

    function toggleMenu() {
        setMenu(menuStatus ? false : true);
    }

    return (
        <div className="menu-container">
            <div className="logo">
                <img src="/images/logo.png" />
            </div>

            <div className={menuStatus ? "menu active" : "menu"}>
                <div className="menu-left">
                    <div className="social">
                        <a href="https://facebook.com" target="_blank">
                            <i className="fa fa-facebook-f"></i>
                        </a>
                        <a href="https://twitter.com" target="_blank">
                            <i className="fa fa-twitter"></i>
                        </a>
                        <a href="https://instagram.com" target="_blank">
                            <i className="fa fa-instagram"></i>
                        </a>
                    </div>
                    <div className="left-menu">
                        <ul>
                            <li>
                                <a>Shop</a>
                            </li>
                            <li>
                                <a>Plan My Kitchen</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div className="menu-right">
                    <div className="right-menu">
                        <ul>
                            <li>
                                <a>About Us</a>
                            </li>
                            <li>
                                <a>Gallery</a>
                            </li>
                        </ul>
                    </div>
                    <div className="order">
                        <button className="ghost white">
                            My Order <i className="fa fa-shopping-cart"></i>
                        </button>
                    </div>
                </div>
            </div>

            <button
                className={
                    menuStatus
                        ? "hamburger hamburger--collapse is-active"
                        : "hamburger hamburger--collapse"
                }
                type="button"
                onClick={toggleMenu}>
                <span className="hamburger-box">
                    <span className="hamburger-inner"></span>
                </span>
            </button>
        </div>
    );
}

export default Menu;
