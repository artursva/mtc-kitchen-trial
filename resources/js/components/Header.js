import React, { useState, useRef, useEffect } from "react";
import TopSlider from "./Main/TopSlider";
import Menu from "./Menu";

function Header() {
    const [isSticky, setSticky] = useState(false);
    const headerInner = useRef(null);
    const handleScroll = () => {

        if (headerInner.current) {

            if (window.scrollY > headerInner.current.scrollHeight) {
                setTimeout(() => {
                    setSticky(true);
                }, 250);
            } else {
                setTimeout(() => {
                    setSticky(false);
                }, 250);
            }
        }
    };

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("scroll", () => handleScroll);
        };
    }, []);

    return (
        <div className="header-container">
            <TopSlider />
            <div className={`header-inner${isSticky ? " sticky" : ""}`} ref={headerInner}>
                <Menu />
            </div>
        </div>
    );
}

export default Header;
