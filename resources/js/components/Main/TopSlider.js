import React from 'react';

import SwiperCore, { Pagination, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

function TopSlider() {
    SwiperCore.use([Pagination, Autoplay]);

    const sliderData = [
        {
            background: "/images/top-slider/slide1.jpg",
            description: "Design and order your new kitchen online today",
            title: "Bespoke & made to measure handmade kitchen design",
            buttonLink: "#"
        },
        {
            background: "/images/top-slider/slide2.jpg",
            description: "Design and order your new bedroom online today",
            title: "Bespoke & made to measure handmade bedroom design",
            buttonLink: "#"
        },
        {
            background: "/images/top-slider/slide3.jpg",
            description: "Design and order your new bathroom online today",
            title: "Bespoke & made to measure handmade bathroom design",
            buttonLink: "#"
        }
    ];

    const slides = sliderData.map((slide, key) => {
        return (
            <SwiperSlide key={key}>
                <div className="slide"
                     style={{ backgroundImage: `url(${slide.background})` }}>
                    <div className="slide-inner">
                        { slide.description ? 
                            <div className="slide-description">{slide.description}</div> 
                        : null }
                        { slide.title ? 
                            <h1 className="shadow">{slide.title}</h1> 
                        : null }
                        { slide.buttonLink ? 
                            <button>Order Now</button> 
                        : null }
                    </div>
                </div>
            </SwiperSlide>
        );
    });

    return (
        <div className="slider-container">
            <Swiper
                pagination={{ clickable: true }}
                spaceBetween={0}
                loop
                autoplay={{ delay: 5000 }}
                slidesPerView={1}>
                {slides}
            </Swiper>
        </div>
    );
}

export default TopSlider;