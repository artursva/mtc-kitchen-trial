import React from 'react';
import LazyLoad from 'react-lazy-load';

function TextImage() {
    return (
        <div className="text-image-container">
            <div className="image" data-aos="fade-right"></div>
            <div className="text-block" data-aos="fade-left">
                <div className="block-inner">
                    <div className="description">Quality Craftmanship from build to delivery</div>
                    <h2>Discover the beauty of a handmade kitchen</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi eget est sit amet sapien venenatis 
                        maximus vel in urna. Nam mauris arcu, feugiat in finibus vitae, sollicitudin id purus. Ut imperdiet, 
                        magna eu pharetra tincidunt, mauris purus ultrices.</p>
                    <button>About Us</button>
                </div>
            </div>
        </div>
    );
}

export default TextImage;