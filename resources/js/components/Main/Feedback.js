import React from "react";

import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from "swiper/react";

import 'swiper/components/navigation/navigation.scss';

function Feedback() {
    SwiperCore.use([Navigation]);

    const feedbackData = [
        {
            title: "Over 35 years experience designing handmade kitchens",
            description:
                "Since my first contact I have received a very high level of customer service " +
                "and advice with my kitchen plans. Ben responded very quickly to all of my emails " +
                "and delivery of the kitchen was as planned.",
            author: "Jane, Dundee"
        },
        {
            title: "Over 35 years experience designing handmade bedrooms",
            description:
                "Since my first contact I have received a very high level of customer service " +
                "and advice with my kitchen plans. Ben responded very quickly to all of my emails " +
                "and delivery of the kitchen was as planned.",
            author: "Arturs, Latvia"
        }
    ];

    const slides = feedbackData.map((slide, key) => {
        return (
            <SwiperSlide key={key} data-aos>
                <div className="feedback-inner">
                    { slide.title ? 
                        <h3>{slide.title}</h3> 
                    : null }
                    { slide.description ? 
                        <p>{slide.description}</p> 
                    : null }
                    { slide.author ? 
                        <div className="author">{slide.author}</div>
                    : null }
                </div>
            </SwiperSlide>
        );
    });

    return (
        <div className="feedback-container">
            <div className="feedback"  data-aos="fade-up">
                <div className="description">What Our Customers Say</div>
                <Swiper
                    spaceBetween={0}
                    loop
                    navigation
                    slidesPerView={1}>
                    {slides}
                </Swiper>
                <button>Frequently Asked Questions</button>
            </div>
        </div>
    );
}

export default Feedback;
