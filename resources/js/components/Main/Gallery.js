import React, { useState } from "react";

function Gallery() {
    const [modalStatus, setStatus] = useState(false);
    const [image, setImage] = useState(null);

    function toggleImage(imageElement) {
        setImage(
            imageElement.target.style.backgroundImage
                .replace('url("', "")
                .replace('")', "")
        );

        setStatus(modalStatus ? false : true);
    }

    return (
        <div className="gallery-container">
            <h3 data-aos="fade-down">Customer Gallery</h3>
            <div className="gallery" data-aos="fade-up">
                <div
                    className="image"
                    style={{
                        backgroundImage: `url(/images/gallery/image-1.jpg)`
                    }}
                    onClick={toggleImage}></div>
                <div
                    className="image"
                    style={{
                        backgroundImage: `url(/images/gallery/image-2.png)`
                    }}
                    onClick={toggleImage}></div>
                <div
                    className="image"
                    style={{
                        backgroundImage: `url(/images/gallery/image-3.jpg)`
                    }}
                    onClick={toggleImage}></div>
                <div
                    className="image"
                    style={{
                        backgroundImage: `url(/images/gallery/image-4.jpg)`
                    }}
                    onClick={toggleImage}></div>
            </div>
            <button>View More</button>

            {modalStatus ? (
                <div className="image-modal" onClick={toggleImage}>
                    <img className="close" src="/images/close.svg"/>
                    <div className="image-container">
                        <img src={image} />
                    </div>
                </div>
            ) : null}
        </div>
    );
}

export default Gallery;
