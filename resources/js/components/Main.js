import React, { useEffect } from "react";
import ReactDOM from "react-dom";

import Header from "./Header";
import Footer from "./Footer";

import TextImage from "./Main/TextImage";
import Feedback from "./Main/Feedback";
import Gallery from "./Main/Gallery";

function Main() {
    useEffect(() => {
        document.getElementById("loading").style.display = "none";

        AOS.init();
    }, []);

    return (
        <div className="content-container">
            <Header />

            <TextImage />
            <Feedback />
            <Gallery />

            <Footer />
        </div>
    );
}

export default Main;

if (document.getElementById("main")) {
    ReactDOM.render(<Main />, document.getElementById("main"));
}
