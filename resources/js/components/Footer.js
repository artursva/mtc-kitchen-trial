import React from "react";

function Footer() {
    return (
        <div className="footer-container">
            <div className="logo" data-aos="fade-down">
                <span></span>
                <img src="/images/logo.png" />
                <span></span>
            </div>
            <div className="footer" data-aos="fade-up">
                <div className="block">
                    <h4>About</h4>
                    <ul>
                        <li>
                            <a>Shop</a>
                        </li>
                        <li>
                            <a>Plan My Kitchen</a>
                        </li>
                        <li>
                            <a>About Us</a>
                        </li>
                        <li>
                            <a>Gallery</a>
                        </li>
                    </ul>
                </div>

                <div className="block">
                    <h4>Service</h4>
                    <ul>
                        <li>
                            <a>FAQ</a>
                        </li>
                        <li>
                            <a>Contact</a>
                        </li>
                        <li>
                            <a>How to Buy</a>
                        </li>
                        <li>
                            <a>Downloads</a>
                        </li>
                    </ul>
                </div>

                <div className="block">
                    <h4>Info</h4>
                    <ul>
                        <li>
                            <a>Delivery</a>
                        </li>
                        <li>
                            <a>Terms</a>
                        </li>
                        <li>
                            <a>Privacy</a>
                        </li>
                    </ul>
                </div>

                <div className="block social">
                    <h4>Follow</h4>
                    <ul>
                        <li>
                            <a href="https://facebook.com" target="_blank">
                                <i className="fa fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com" target="_blank">
                                <i className="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://instagram.com" target="_blank">
                                <i className="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="copyrights" data-aos="fade-up">
                Copyright Online MTC Home Kitchens 2018 - All rights reserved.
                <br />
                Responsive website design, Development & Hosting by mtc.
            </div>
        </div>
    );
}

export default Footer;
