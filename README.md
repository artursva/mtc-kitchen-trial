# MTC Kitchen Trial

- Used Laravel + React.
- Spent time: 6.5h at all.
- As i can't export assets from XD, used my own images, found in google. :)
- Ignored Helvetica font (because it's paid font). Replaced with the same, traditional Roboto font.
- Created demo landing page. All links are inactive.
- Let me know if need to add or change something (no matter what - i can do anything :P)
- Builded Demo URL: [https://mtc.takeprofiteasy.com](https://mtc.takeprofiteasy.com)